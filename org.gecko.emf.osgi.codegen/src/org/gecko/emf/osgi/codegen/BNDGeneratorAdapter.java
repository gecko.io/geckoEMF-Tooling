/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.emf.osgi.codegen;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.codegen.ecore.CodeGenEcorePlugin;
import org.eclipse.emf.codegen.ecore.generator.GeneratorAdapterFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapter;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.Monitor;

/**
 * EMF codegen generator adapter that is responsible to generate an adequate Bnd-project setup with
 * natures, builders and bnd.bnd file
 * @author Mark Hoffmann
 * @since 25.07.2017
 */
public class BNDGeneratorAdapter extends GenModelGeneratorAdapter {

	protected static final int MODEL_BND_BND_ID = 30;
	private static final String BND_NATURE_ID = "bndtools.core.bndnature";
	private static final String BND_BUILDER_ID = "bndtools.core.bndbuilder";

	private static final JETEmitterDescriptor[] JET_EMITTER_DESCRIPTORS =
		{
				new JETEmitterDescriptor("model/bnd.bndjet", "org.gecko.emf.osgi.codegen.templates.model.BndBnd"),
		};

	public BNDGeneratorAdapter(GeneratorAdapterFactory generatorAdapterFactory) {
		super(generatorAdapterFactory);
	}
	/**
	 * Returns the set of <code>JETEmitterDescriptor</code>s used by the adapter. The contents of the returned array
	 * should never be changed. Rather, subclasses may override this method to return a different array altogether.
	 */
	protected JETEmitterDescriptor[] getJETEmitterDescriptors()
	{
		List<JETEmitterDescriptor> descs = new LinkedList<JETEmitterDescriptor>();
		descs.addAll(Arrays.asList(super.getJETEmitterDescriptors()));
		descs.add(JET_EMITTER_DESCRIPTORS[0]);
		return descs.toArray(new JETEmitterDescriptor[descs.size()]);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapter#generateModel(java.lang.Object, org.eclipse.emf.common.util.Monitor)
	 */
	@Override
	protected Diagnostic generateModel(Object object, Monitor monitor)
	{
		monitor.beginTask("", 7);

		GenModel genModel = (GenModel)object;
		message = CodeGenEcorePlugin.INSTANCE.getString("_UI_GeneratingPackages_message");
		monitor.subTask(message);

		ensureProjectExists
		(genModel.getModelDirectory(), genModel, MODEL_PROJECT_TYPE, genModel.isUpdateClasspath(), createMonitor(monitor, 1));

		generateModelPluginClass(genModel, monitor);
		if (genModel.isOSGiCompatible()) {
			generateModelBnd(genModel, monitor);
		} else {
			generateModelBuildProperties(genModel, monitor);
			generateModelManifest(genModel, monitor);
		}
		generateModelPluginProperties(genModel, monitor);
		generateModelModule(genModel, monitor);
		return Diagnostic.OK_INSTANCE;
	}

	/**
	 * Generate a bnd project descriptor and bnd.bnd file
	 * @param genModel the generator model
	 * @param monitor the progress monitor
	 */
	protected void generateModelBnd(GenModel genModel, Monitor monitor)
	{
		try {
			if (genModel.hasPluginSupport() && !genModel.sameModelEditProject() && !genModel.sameModelEditorProject())
			{
				if (genModel.isBundleManifest() && genModel.isOSGiCompatible() )
				{
					message = String.format("Generating Bnd project setup for model '%s'",  genModel.getModelName());
					monitor.subTask(message);

					// Do allow an existing bnd.bnd to be overwritten, since it may have been created as part of an empty EMF project.
					//
					generateText
					(genModel.getModelProjectDirectory() + "/bnd.bnd",
							getJETEmitter(getJETEmitterDescriptors(), MODEL_BND_BND_ID),
							null,
							genModel.isUpdateClasspath(),
							MANIFEST_ENCODING,
							createMonitor(monitor, 1));
				}
				else
				{
					monitor.worked(1);
				}

			}
			else
			{
				monitor.worked(2);
			}
		} catch (Exception e) {
			System.err.println("ERROR GENERTING BND FILE");
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Changes to the project settings to Bnd, for the given project name.
	 * It removes all PDE similar natures if keepPDE parameter is <code>false</code>
	 * @param projectName the project name to be changed
	 * @param keepPDE <code>true</code>, to keep PDE nature and builder
	 */
	protected void changeProjectToBnd(String projectName, boolean keepPDE) {
		try {
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			IProject project = workspace.getRoot().getProject(projectName);
			IProjectDescription description = project.getDescription();
			List<String> natures = Arrays.asList(description.getNatureIds()).stream()
					.filter(n->keepPDE || !n.startsWith("org.eclipse.pde"))
					.collect(Collectors.toList());
			if (!natures.contains(BND_NATURE_ID)) {
				natures.add(BND_NATURE_ID);
			}
			description.setNatureIds(natures.toArray(new String[natures.size()]));
			List<ICommand> buildCommands = Arrays.asList(description.getBuildSpec()).stream()
					.filter(cmd->keepPDE || !cmd.getBuilderName().startsWith("org.eclipse.pde"))
					.collect(Collectors.toList());
			Optional<ICommand> bndBuilder = buildCommands.stream()
					.filter(cmd->cmd.getBuilderName().equals(BND_BUILDER_ID))
					.findFirst();
			if (!bndBuilder.isPresent()) {
				ICommand bndNewCmd = description.newCommand();
				bndNewCmd.setBuilderName(BND_BUILDER_ID);
				buildCommands.add(bndNewCmd);
			}
			description.setBuildSpec(buildCommands.toArray(new ICommand[buildCommands.size()]));
			project.setDescription(description, new NullProgressMonitor());
		} catch (CoreException e) {
			// Something went wrong
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.codegen.ecore.generator.AbstractGeneratorAdapter#doPostGenerate(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected Diagnostic doPostGenerate(Object object, Object projectType) {
		GenModel genModel = (GenModel)object;
		if (genModel.isOSGiCompatible()) {
			changeProjectToBnd(genModel.getModelProjectDirectory(), false);
		}
		return super.doPostGenerate(object, projectType);
	}

}
