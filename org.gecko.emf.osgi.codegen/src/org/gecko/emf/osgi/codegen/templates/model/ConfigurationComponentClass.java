package org.gecko.emf.osgi.codegen.templates.model;

import java.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;

public class ConfigurationComponentClass
{
  protected static String nl;
  public static synchronized ConfigurationComponentClass create(String lineSeparator)
  {
    nl = lineSeparator;
    ConfigurationComponentClass result = new ConfigurationComponentClass();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = "/**" + NL + " * Copyright (c) 2012 - 2018 Data In Motion and others." + NL + " * All rights reserved. " + NL + " * " + NL + " * This program and the accompanying materials are made available under the terms of the " + NL + " * Eclipse Public License v1.0 which accompanies this distribution, and is available at" + NL + " * http://www.eclipse.org/legal/epl-v10.html" + NL + " * " + NL + " * Contributors:" + NL + " *     Data In Motion - initial API and implementation";
  protected final String TEXT_3 = NL + " * ";
  protected final String TEXT_4 = NL + " */" + NL + "package ";
  protected final String TEXT_5 = ".configuration;" + NL;
  protected final String TEXT_6 = NL + NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * The <b>EPackageConfiguration</b> and <b>ResourceFactoryConfigurator</b> for the model." + NL + " * The package will be registered into a OSGi base model registry." + NL + " * <!-- end-user-doc -->";
  protected final String TEXT_7 = NL + " * <!-- begin-model-doc -->" + NL + " * ";
  protected final String TEXT_8 = NL + " * <!-- end-model-doc -->";
  protected final String TEXT_9 = NL + " * @see EPackageConfigurator" + NL + " * @see ResourceFactoryConfigurator" + NL + " * @generated" + NL + " */" + NL + "@Component(name=\"";
  protected final String TEXT_10 = "Configurator\", service= {EPackageConfigurator.class, ResourceFactoryConfigurator.class})" + NL + "@EMFModel(emf_model_name=";
  protected final String TEXT_11 = ".eNAME, emf_model_nsURI={";
  protected final String TEXT_12 = ".eNS_URI}, emf_model_version=\"1.0\")" + NL + "@RequireEMF" + NL + "@ProvideEMFModel(name = ";
  protected final String TEXT_13 = ".eNAME, nsURI = { ";
  protected final String TEXT_14 = ".eNS_URI }, version = \"1.0\" )" + NL + "@ProvideEMFResourceConfigurator( name = ";
  protected final String TEXT_15 = ".eNAME,";
  protected final String TEXT_16 = NL + "\tcontentType = { \"";
  protected final String TEXT_17 = "\" },";
  protected final String TEXT_18 = NL + "\tcontentType = { \"\" },";
  protected final String TEXT_19 = " ";
  protected final String TEXT_20 = NL + "\tfileExtension = {" + NL + "\t\"";
  protected final String TEXT_21 = "\"";
  protected final String TEXT_22 = "," + NL + " \t\"";
  protected final String TEXT_23 = NL + " \t},";
  protected final String TEXT_24 = " " + NL + "\tversion = \"1.0\"" + NL + ")" + NL + "public class ";
  protected final String TEXT_25 = "ConfigurationComponent implements EPackageConfigurator, ResourceFactoryConfigurator" + NL + "{" + NL + "" + NL + "\t/* " + NL + "\t * (non-Javadoc)" + NL + "\t * @see org.gecko.emf.osgi.ResourceFactoryConfigurator#configureResourceFactory(org.eclipse.emf.ecore.resource.Resource.Factory.Registry)" + NL + "\t * @generated" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void configureResourceFactory(Registry registry) {" + NL + "\t\t";
  protected final String TEXT_26 = "registry.getExtensionToFactoryMap().put(\"";
  protected final String TEXT_27 = "\", new ";
  protected final String TEXT_28 = "());";
  protected final String TEXT_29 = " " + NL + "\t\t";
  protected final String TEXT_30 = "registry.getContentTypeToFactoryMap().put(\"";
  protected final String TEXT_31 = " " + NL + "\t}" + NL + "\t" + NL + "\t/* " + NL + "\t * (non-Javadoc)" + NL + "\t * @see org.gecko.emf.osgi.ResourceFactoryConfigurator#unconfigureResourceFactory(org.eclipse.emf.ecore.resource.Resource.Factory.Registry)" + NL + "\t * @generated" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void unconfigureResourceFactory(Registry registry) {" + NL + "\t\t";
  protected final String TEXT_32 = "registry.getExtensionToFactoryMap().remove(\"";
  protected final String TEXT_33 = "\");";
  protected final String TEXT_34 = "registry.getContentTypeToFactoryMap().remove(\"";
  protected final String TEXT_35 = " " + NL + "\t}" + NL + "\t" + NL + "\t/* " + NL + "\t * (non-Javadoc)" + NL + "\t * @see org.gecko.emf.osgi.EPackageRegistryConfigurator#configureEPackage(org.eclipse.emf.ecore.EPackage.Registry)" + NL + "\t * @generated" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void configureEPackage(org.eclipse.emf.ecore.EPackage.Registry registry) {" + NL + "\t\tregistry.put(";
  protected final String TEXT_36 = ".eNS_URI, ";
  protected final String TEXT_37 = ".eINSTANCE);" + NL + "\t}" + NL + "\t" + NL + "\t/* " + NL + "\t * (non-Javadoc)" + NL + "\t * @see org.gecko.emf.osgi.EPackageRegistryConfigurator#unconfigureEPackage(org.eclipse.emf.ecore.EPackage.Registry)" + NL + "\t * @generated" + NL + "\t */" + NL + "\t@Override" + NL + "\tpublic void unconfigureEPackage(org.eclipse.emf.ecore.EPackage.Registry registry) {" + NL + "\t\tregistry.remove(";
  protected final String TEXT_38 = ".eNS_URI);" + NL + "\t}" + NL + "\t" + NL + "}";
  protected final String TEXT_39 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */

    final GenPackage genPackage = (GenPackage)((Object[])argument)[0]; final GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    }}
    stringBuffer.append(TEXT_4);
    stringBuffer.append(genPackage.getInterfacePackageName());
    stringBuffer.append(TEXT_5);
    genModel.markImportLocation(stringBuffer, genPackage);
    genModel.addImport("org.osgi.service.component.annotations.Component");
    genModel.addImport("org.eclipse.emf.ecore.resource.Resource.Factory.Registry");
    genModel.addImport("org.gecko.emf.osgi.EPackageConfigurator");
    genModel.addImport("org.gecko.emf.osgi.ResourceFactoryConfigurator");
    genModel.addImport("org.gecko.emf.osgi.annotation.EMFModel");
    genModel.addImport("org.gecko.emf.osgi.annotation.provide.ProvideEMFModel");
    genModel.addImport("org.gecko.emf.osgi.annotation.provide.ProvideEMFResourceConfigurator");
    genModel.addImport("org.gecko.emf.osgi.annotation.require.RequireEMF");
    genModel.addImport(genPackage.getQualifiedPackageInterfaceName());
    if (!GenResourceKind.NONE_LITERAL.equals(genPackage.getResource())) {
    genModel.addImport(genPackage.getQualifiedResourceFactoryClassName());
    }
    stringBuffer.append(TEXT_6);
    if (genPackage.hasDocumentation()) {
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genPackage.getDocumentation(genModel.getIndentation(stringBuffer)));
    stringBuffer.append(TEXT_8);
    }
    stringBuffer.append(TEXT_9);
    stringBuffer.append(genPackage.getPrefix());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_15);
    if (genPackage.getContentTypeIdentifier() != null) {
    stringBuffer.append(TEXT_16);
    stringBuffer.append(genPackage.getContentTypeIdentifier());
    stringBuffer.append(TEXT_17);
    } else {
    stringBuffer.append(TEXT_18);
    }
    stringBuffer.append(TEXT_19);
    if (!genPackage.getFileExtensionList().isEmpty()) {
    Iterator<String> fileExtensionIterator = genPackage.getFileExtensionList().iterator(); if (fileExtensionIterator.hasNext()) { String fileExtension = fileExtensionIterator.next();
    stringBuffer.append(TEXT_20);
    stringBuffer.append(fileExtension);
    stringBuffer.append(TEXT_21);
    while(fileExtensionIterator.hasNext()) { fileExtension = fileExtensionIterator.next();
    stringBuffer.append(TEXT_22);
    stringBuffer.append(fileExtension);
    stringBuffer.append(TEXT_21);
    }
    stringBuffer.append(TEXT_23);
    }
    stringBuffer.append(TEXT_19);
    }
    stringBuffer.append(TEXT_24);
    stringBuffer.append(genPackage.getPrefix());
    stringBuffer.append(TEXT_25);
    if (!genPackage.getFileExtensionList().isEmpty() && !GenResourceKind.NONE_LITERAL.equals(genPackage.getResource())) {
    for (String fileExtension : genPackage.getFileExtensionList()) {
    stringBuffer.append(TEXT_26);
    stringBuffer.append(fileExtension);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_28);
    }
    }
    stringBuffer.append(TEXT_29);
    if (genPackage.getContentTypeIdentifier() != null && !GenResourceKind.NONE_LITERAL.equals(genPackage.getResource())) {
    stringBuffer.append(TEXT_30);
    stringBuffer.append(genPackage.getContentTypeIdentifier());
    stringBuffer.append(TEXT_27);
    stringBuffer.append(genPackage.getResourceFactoryClassName());
    stringBuffer.append(TEXT_28);
    }
    stringBuffer.append(TEXT_31);
    if (!genPackage.getFileExtensionList().isEmpty() && !GenResourceKind.NONE_LITERAL.equals(genPackage.getResource())) {
    for (String fileExtension : genPackage.getFileExtensionList()) {
    stringBuffer.append(TEXT_32);
    stringBuffer.append(fileExtension);
    stringBuffer.append(TEXT_33);
    }
    }
    stringBuffer.append(TEXT_29);
    if (genPackage.getContentTypeIdentifier() != null && !GenResourceKind.NONE_LITERAL.equals(genPackage.getResource())) {
    stringBuffer.append(TEXT_34);
    stringBuffer.append(genPackage.getContentTypeIdentifier());
    stringBuffer.append(TEXT_33);
    }
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(genPackage.getPackageInterfaceName());
    stringBuffer.append(TEXT_38);
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_39);
    return stringBuffer.toString();
  }
}
