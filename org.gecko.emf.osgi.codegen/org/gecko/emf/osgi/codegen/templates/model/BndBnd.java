package org.gecko.emf.osgi.codegen.templates.model;

import java.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;

public class BndBnd
{
  protected static String nl;
  public static synchronized BndBnd create(String lineSeparator)
  {
    nl = lineSeparator;
    BndBnd result = new BndBnd();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "-buildpath: \\" + NL + "\tosgi.annotation;version=7.0.0,\\" + NL + "\tosgi.core;version=6.0,\\" + NL + "\tosgi.cmpn;version=7.0,\\" + NL + "\torg.gecko.emf.osgi.annotation;version=latest,\\" + NL + "\torg.gecko.emf.osgi.api;version=latest,\\" + NL + "\torg.eclipse.emf.common,\\" + NL + "\torg.eclipse.emf.ecore,\\" + NL + "\torg.eclipse.emf.ecore.xmi" + NL + "" + NL + "-testpath: \\" + NL + "\t${junit}" + NL + "\t" + NL + "-includeresource model/=model,\\" + NL + "\tplugin.properties=plugin.properties" + NL;
  protected final String TEXT_2 = NL + "javac.source: 1.5" + NL + "javac.target: 1.5";
  protected final String TEXT_3 = NL + "javac.source: 1.6" + NL + "javac.target: 1.6";
  protected final String TEXT_4 = NL + "javac.source: 1.7" + NL + "javac.target: 1.7";
  protected final String TEXT_5 = NL + "javac.source: 1.8" + NL + "javac.target: 1.8";
  protected final String TEXT_6 = NL + NL + "Manifest-Version: 1.0" + NL + "Bundle-ManifestVersion: 2" + NL + "Bundle-Name: %pluginName" + NL + "#Bundle-SymbolicName: ";
  protected final String TEXT_7 = NL + "Bundle-Version: 1.0.0.SNAPSHOT" + NL + "Bundle-ClassPath: ";
  protected final String TEXT_8 = ".jar";
  protected final String TEXT_9 = ".";
  protected final String TEXT_10 = NL + "Bundle-Activator: ";
  protected final String TEXT_11 = "$Implementation";
  protected final String TEXT_12 = "$Activator";
  protected final String TEXT_13 = NL + "Bundle-Vendor: %providerName" + NL + "Bundle-Localization: plugin" + NL + "#Provide-Capability: emf.model.config;emf.model=";
  protected final String TEXT_14 = NL;
  protected final String TEXT_15 = NL + "Private-Package: \\" + NL + "\t";
  protected final String TEXT_16 = ".configuration";
  protected final String TEXT_17 = ",\\" + NL + " \t";
  protected final String TEXT_18 = NL + "Export-Package: \\" + NL + "\t";
  protected final String TEXT_19 = NL + "Import-Package: \\" + NL + "\t,\\" + NL + "\t!";
  protected final String TEXT_20 = ".*,\\";
  protected final String TEXT_21 = NL + " \t!";
  protected final String TEXT_22 = NL + " \t*";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */

    GenModel genModel = (GenModel)argument;
    stringBuffer.append(TEXT_1);
    if (genModel.getComplianceLevel() == GenJDKLevel.JDK50_LITERAL) {
    stringBuffer.append(TEXT_2);
    } else if (genModel.getComplianceLevel() == GenJDKLevel.JDK60_LITERAL) {
    stringBuffer.append(TEXT_3);
    } else if (genModel.getComplianceLevel() == GenJDKLevel.JDK70_LITERAL) {
    stringBuffer.append(TEXT_4);
    } else if (genModel.getComplianceLevel() == GenJDKLevel.JDK80_LITERAL) {
    stringBuffer.append(TEXT_5);
    }
    stringBuffer.append(TEXT_6);
    stringBuffer.append(genModel.getModelPluginID());
    stringBuffer.append(TEXT_7);
    if (genModel.isRuntimeJar()) {
    stringBuffer.append(genModel.getModelPluginID());
    stringBuffer.append(TEXT_8);
    }else{
    stringBuffer.append(TEXT_9);
    }
    if (genModel.hasModelPluginClass()) {
    stringBuffer.append(TEXT_10);
    stringBuffer.append(genModel.getQualifiedModelPluginClassName());
    stringBuffer.append(TEXT_11);
    if (genModel.isOSGiCompatible()) {
    stringBuffer.append(TEXT_12);
    }
    }
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genModel.getModelName().toLowerCase());
    stringBuffer.append(TEXT_14);
    Iterator<GenPackage> genPackageIterator = genModel.getAllGenPackagesWithClassifiers().iterator(); if (genPackageIterator.hasNext()) { GenPackage pack = genPackageIterator.next();
    stringBuffer.append(TEXT_15);
    stringBuffer.append(pack.getInterfacePackageName());
    stringBuffer.append(TEXT_16);
    while(genPackageIterator.hasNext()) { pack = genPackageIterator.next();
    stringBuffer.append(TEXT_17);
    stringBuffer.append(pack.getInterfacePackageName());
    stringBuffer.append(TEXT_16);
    }
    }
    stringBuffer.append(TEXT_14);
    Iterator<String> packagesIterator = genModel.getModelQualifiedPackageNames().iterator(); if (packagesIterator.hasNext()) { String pack = packagesIterator.next();
    stringBuffer.append(TEXT_18);
    stringBuffer.append(pack);
    while(packagesIterator.hasNext()) { pack = packagesIterator.next();
    stringBuffer.append(TEXT_17);
    stringBuffer.append(pack);
    }
    }
    stringBuffer.append(TEXT_14);
    Iterator<GenPackage> genPackageIteratorImport = genModel.getAllGenPackagesWithClassifiers().iterator(); if (genPackageIteratorImport.hasNext()) { GenPackage packImport = genPackageIteratorImport.next();
    stringBuffer.append(TEXT_19);
    stringBuffer.append(packImport.getInterfacePackageName());
    stringBuffer.append(TEXT_20);
    while(genPackageIteratorImport.hasNext()) { packImport = genPackageIteratorImport.next();
    stringBuffer.append(TEXT_21);
    stringBuffer.append(packImport.getInterfacePackageName());
    stringBuffer.append(TEXT_20);
    }
    stringBuffer.append(TEXT_22);
    }
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_14);
    return stringBuffer.toString();
  }
}
