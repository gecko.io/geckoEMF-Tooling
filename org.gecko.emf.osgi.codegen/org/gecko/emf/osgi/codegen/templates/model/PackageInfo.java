package org.gecko.emf.osgi.codegen.templates.model;

import org.eclipse.emf.codegen.ecore.genmodel.*;

public class PackageInfo
{
  protected static String nl;
  public static synchronized PackageInfo create(String lineSeparator)
  {
    nl = lineSeparator;
    PackageInfo result = new PackageInfo();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "@org.osgi.annotation.versioning.Version(\"1.0.0\")" + NL + "package ";
  protected final String TEXT_2 = ";";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */

    final GenPackage genPackage = (GenPackage)((Object[])argument)[0];
    stringBuffer.append(TEXT_1);
    stringBuffer.append(genPackage.getInterfacePackageName());
    stringBuffer.append(TEXT_2);
    return stringBuffer.toString();
  }
}
