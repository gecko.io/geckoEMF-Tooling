# Gecko EMF-Tooling

**THIS PROJECT IS DEPRECATED AND NOT USED ANYMORE**

It moved to Github:
https://github.com/geckoprojects-org/org.gecko.emf

Please use the EMF Generator instead:
https://github.com/geckoprojects-org/org.gecko.emf/tree/main/org.gecko.emf.osgi.codegen

The artifacts can be found here:
https://repo1.maven.org/maven2/org/geckoprojects/emf/

**THIS PROJECT IS DEPRECATED AND NOT USED ANYMORE**



This project contains Eclipse IDE Tooling for the GeckoEMF project.

It support generation of:

- EPackageConfigurators
- ResourceFactoryConfigurators
- bnd file
- project setup for builders and natures

These configrurators are whiteboard services, that are used by the OSGi based EPackageRegistry from geckoEMF

## How to use it

There is no additional button the generate the code. You can use the "*Generate Model*" menu as usual. The tooling is activated when you set the "*OSGi Compatible*" option in the genmodel - All section to **true**

## Installation

The stable builds are available via P2 Update site at:

http://devel.data-in-motion.biz/public/update/repository/gecko/release/geckoEMF.eclipse.tooling/

For those who are interested in the latest builds use the snapshot repository location:

http://devel.data-in-motion.biz/public/update/repository/gecko/snapshot/geckoEMFTooling/

## How it works

This tooling makes use of the standard JET-based EMF Generator and extents it. For that all the EMF Generator extension are used.

# Contact

Feel free to file Bugs and for support contact us directly via the mailing list at gecko-emf@googlegroups.com

